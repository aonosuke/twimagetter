module ImagesHelper
    def comp_commercial_at(text)
        text[0] == '@' ? text : '@' + text
    end
    
    def tweet_url(tweet_info)
        'https://twitter.com/' + tweet_info["user"] + '/status/' + tweet_info["id"]
    end
end