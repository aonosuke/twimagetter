Rails.application.routes.draw do
  root 'images#home'
  get  '/user_images_index',    to: 'images#user_images_index'
  get  '/keyword_images_index', to: 'images#keyword_images_index'
  post '/batch_download',       to: 'images#batch_download'
  get  '/individ_download',     to: 'images#individ_download'
  get  '/send_zip_to_client',   to: 'images#send_zip_to_client'
  get  '/send_image_to_client', to: 'images#send_image_to_client'
end
