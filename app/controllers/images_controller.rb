class ImagesController < ApplicationController
  
  require 'open-uri'

  def home
    @tweet ||= TweetModel.new
  end
  
  def user_images_index
    @tweet ||= TweetModel.new(id: params[:id])
    begin
      @tweet.search_user_images
      render "index"
    rescue Twitter::Error::NotFound
      flash[:danger] = "Your search user does not exist"
      redirect_to root_url
    end
  end
  
  def keyword_images_index
    @tweet ||= TweetModel.new(keyword: params[:keyword], result_type: params[:result_type])
    @tweet.search_keyword_images
    render "index"
  end
  
  def batch_download
    @tweet ||= TweetModel.new(tweet_params)
    @send_zip_to_client_url = send_zip_to_client_path(:zip => @tweet.download_to_server)
    render :partial => "send_zip_to_client"
  end
  
  def individ_download
    @send_image_to_client_url = send_image_to_client_path(:image => params[:image])
    render :partial => "send_image_to_client"
  end
  
  def send_zip_to_client
    send_zip params[:zip]
  end
  
  def send_image_to_client
    send_image params[:image]
  end
  
  private
  
    def tweet_params
      params.require(:tweet_model).permit(:id, :keyword, images: [])
    end
    
    def send_zip(zip)
      send_file(zip, :filename => File.basename(zip), :type => 'application/zip', :disposition => 'attachment', :x_sendfile => true)
    end
    
    def send_image(image)
      open(image) do |f|
        send_data(f.read, :filename => File.basename(image), :disposition => 'attachment')
      end
    end
    
end
