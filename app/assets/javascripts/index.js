$(function() {
    $('input:checkbox').change(function() {
        var cnt = $('#images_area input:checkbox:checked').length;
        $('.select_count').text('(' + cnt + '/' + $('#images_area :input').length + ')');
        if (cnt == 0) {
            $('#submit_download').prop('disabled', true);
        }else{
            $('#submit_download').prop('disabled', false);
        }
    }).trigger('change');
});

$(function() {
    $('#images_all').on('click', function() {
        $('.checkbox').prop('checked', this.checked);
    });

    $('.checkbox').on('click', function() {
        if ($('#images_area :checked').length == $('#images_area :input').length){
            $('#images_all').prop('checked', 'checked');
        }else{
            $('#images_all').prop('checked', false);
        }
    });
});

$(function() {
    $('#submit_download').on('click', function() {
        $('#loading_image').show();
    });
});