class TweetModel
    include ActiveModel::Model
    
    require 'fileutils'
    require 'open-uri'
    require 'zip'
    
    attr_accessor :id, :keyword, :images, :user_info, :tweets_info, :result_type
    
    GET_USER_TWEETS_MAX_COUNT  = 200
    GET_KEYWORD_TWEETS_MAX_COUNT  = 100
    SAVE_IMAGES_ROOT_DIR  = "/tmp/twimagetter/"
    SLASH                 = "/"
    EXTENSION_ZIP         = "\.zip"
    
    def initialize(args={})
      @id = args[:id] || ""
      @keyword = args[:keyword] || ""
      @images = args[:images] || []
      @user_info = {}
      @tweets_info = []
      @result_type = args[:result_type] || "recent"
    end
    
    def search_user_images
      get_user_info
      get_user_images
    end
    
    def search_keyword_images
      get_keyword_images
    end
    
    def download_to_server
      save_images_dir_path = create_dir
      save save_images_dir_path
      output_zip_name = create_zip save_images_dir_path
      FileUtils.rm_rf save_images_dir_path
      return output_zip_name
    end
    
    private
      
      def client
        @client ||= Twitter::REST::Client.new do |config| 
          config.consumer_key = Rails.application.secrets.user_consumer_key
          config.consumer_secret = Rails.application.secrets.user_consumer_secret
          config.bearer_token = Rails.application.secrets.user_bearer_token
        end
      end

      def get_user_info
        self.user_info = extract_user_info raw_user_info
      end
      
      def raw_user_info
        client.user(self.id)
      end
    
      def extract_user_info(raw_user_info)
        { "name" => raw_user_info.name,
          "description" => raw_user_info.description,
          "url" => raw_user_info.url.to_s,
          "profile_image_url" => raw_user_info.profile_image_url.to_s
        }
      end
      
      def get_user_images
        extract_images user_tweets
      end
    
      def user_tweets
        collect_with_max_id do |max_id|
          options = {count: GET_USER_TWEETS_MAX_COUNT, include_rts: false}
          options[:max_id] = max_id unless max_id.nil?
          client.user_timeline(self.id, options)
        end
      end
      
      def collect_with_max_id(collection=[], max_id=nil, &block)
        response = yield(max_id)
        collection += response
        response.empty? ? collection.flatten : collect_with_max_id(collection, response.last.id - 1, &block)
      end
      
      def get_keyword_images
        extract_images keyword_tweets
      end
      
      def keyword_tweets
        client.search(self.keyword, keyword_search_options).take(1500)
      end
      
      def keyword_search_options
        {result_type: self.result_type, filter: "images", exclude: "retweets"}
      end
      
      def extract_images(tweets)
        tweets.each do |tweet|
          get_image_url(tweet)
        end
      end
      
      def get_image_url(tweet)
        if tweet.media.any?
          tweet.media.each do |media|
            self.images << get_media_url(media)
            self.tweets_info << extract_tweet_info(tweet)
          end
        end
      end
      
      def get_media_url(media)
        media.media_url.to_s
      end
      
      def extract_tweet_info(tweet)
        { "user" => tweet.user.screen_name.to_s,
          "id"   => tweet.id.to_s
        }
      end
      
      def create_dir
        now = Time.now
        dir_path = SAVE_IMAGES_ROOT_DIR + now.strftime("%Y%m%d%H%M%S") + now.usec.to_s + SLASH
        FileUtils.mkdir_p(dir_path)
        return dir_path
      end
      
      def save(save_images_dir_path)
        self.images.each do |image|
          output_file_name = save_images_dir_path + File.basename(image)
          open(output_file_name, 'wb') do |output|
            open(image) do |data|
              output.write(data.read)
            end
          end
        end
      end
      
      def create_zip(save_images_dir_path)
        output_zip_name = SAVE_IMAGES_ROOT_DIR + (self.id.blank? ? self.id : (self.id + "_")) + (self.keyword.blank? ? self.keyword : (self.keyword + "_")) + File.basename(save_images_dir_path) + EXTENSION_ZIP
        entries = Dir.entries(save_images_dir_path) - %w(. ..)
        ::Zip::File.open(output_zip_name, ::Zip::File::CREATE) do |io|
          write_entries entries, '', io, save_images_dir_path
        end
        return output_zip_name
      end
      
      def write_entries(entries, path, io, input_dir)
        entries.each do |e|
          zip_file_path = path == '' ? e : File.join(path, e)
          disk_file_path = File.join(input_dir, zip_file_path)
          if File.directory? disk_file_path
            recursively_deflate_directory(disk_file_path, io, zip_file_path)
          else
            put_into_archive(disk_file_path, io, zip_file_path)
          end
        end
      end
      
      def recursively_deflate_directory(disk_file_path, io, zip_file_path)
        io.mkdir zip_file_path
        subdir = Dir.entries(disk_file_path) - %w(. ..)
        write_entries subdir, zip_file_path, io
      end
      
      def put_into_archive(disk_file_path, io, zip_file_path)
        io.get_output_stream(zip_file_path) do |f|
          f.write(File.open(disk_file_path, 'rb').read)
        end
      end
      
end